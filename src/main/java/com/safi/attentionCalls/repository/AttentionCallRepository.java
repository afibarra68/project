package com.safi.attentionCalls.repository;

import com.safi.attentionCalls.domain.AttentionCall;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface AttentionCallRepository extends CrudRepository<AttentionCall, Integer> {
    //Optional<AttentionCall> findById(String serial);
}

