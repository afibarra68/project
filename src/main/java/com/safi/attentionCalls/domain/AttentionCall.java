package com.safi.attentionCalls.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class AttentionCall {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int idCall;

    //@ManyToOne
    // private File file;
    //@ManyToOne
    //private Users users;

    private String situation;
    private String descriptionCausal;
    private String impact;
    private String effect;
    private String commitments;

    private String dateAttention;


    public AttentionCall(String situation, String descriptionCausal, String impact, String effect, String commitments, String dateAttention) {
        this.situation = situation;
        this.descriptionCausal = descriptionCausal;
        this.impact = impact;
        this.effect = effect;
        this.commitments = commitments;
        this.dateAttention = dateAttention;
    }
}
