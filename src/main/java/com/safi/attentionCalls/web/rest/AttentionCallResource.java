package com.safi.attentionCalls.web.rest;


import com.safi.attentionCalls.domain.AttentionCall;
import com.safi.attentionCalls.service.IAttentionCallService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AttentionCallResource {

    //@Autowired
    IAttentionCallService attentionCallService;

    public AttentionCallResource(IAttentionCallService attentionCallService) {
        this.attentionCallService = attentionCallService;
    }

    //Create
    @PostMapping("/attentioncalls")
    public ResponseEntity create(@RequestBody AttentionCall attentionCall) {
        return attentionCallService.create(attentionCall);
    }
    //Get
    @GetMapping("/attentioncalls")
    public Iterable<AttentionCall> read() {
        return attentionCallService.read();
    }
    //Put
    @PutMapping("/attentioncalls")
    public AttentionCall update(@RequestBody AttentionCall attentionCall) {
        return attentionCallService.update(attentionCall);
    }
    //Get only one
    @GetMapping("/bike/{id}")
    public Optional<AttentionCall> getById(@PathVariable Integer id) {
        return attentionCallService.getById(id);
    }
}
