package com.safi.attentionCalls.service;

import com.safi.attentionCalls.domain.AttentionCall;
import com.safi.attentionCalls.repository.AttentionCallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IAttentionCallServiceImp implements IAttentionCallService{

    //@Autowired
    private final AttentionCallRepository attentionCallRepository;

    public IAttentionCallServiceImp(AttentionCallRepository attentionCallRepository) {
        this.attentionCallRepository = attentionCallRepository;
    }



    @Override
    public ResponseEntity create(AttentionCall attentionCall) {
        return new ResponseEntity(attentionCallRepository.save(attentionCall), HttpStatus.OK);
    }

    @Override
    public Iterable<AttentionCall> read() {
        return attentionCallRepository.findAll();
    }

    @Override
    public AttentionCall update(AttentionCall attentionCall) {
        return null;
    }

    @Override
    public Optional<AttentionCall> getById(Integer id) {
        return Optional.empty();
    }
}
