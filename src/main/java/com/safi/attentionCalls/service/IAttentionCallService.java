package com.safi.attentionCalls.service;

import com.safi.attentionCalls.domain.AttentionCall;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IAttentionCallService {

    public ResponseEntity create(AttentionCall attentionCall);

    public Iterable<AttentionCall> read();

    public AttentionCall update(AttentionCall attentionCall);

    public Optional<AttentionCall> getById(Integer id);


}
